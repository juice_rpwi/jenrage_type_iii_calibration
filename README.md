# JENRAGE Type III Calibration

## Goal

Calibration JUICE/RPWI/JENRAGE using WIND/Waves, STEREO-A/Waves and Solar-Orbiter/RPW observations. 

## Method

We use simultaneous observations of Type III Solar Radio Bursts by the radio instrument onboard four spacecraft (JUICE, Wind, STEREO-A and Solar-Orbiter). This method has been successfully applied for cross-calibrating Wind, STEREO-A and Solar-Orbiter (Vecchio et al. 2021). 

Each radio receiver has its own spectral coverage and sampling scheme. We select JUICE/RPWI/JENRAGE frequency channels that are not dominated by RFI. We then compare the JUICE radio flux density time-series to the observations from the 3 other spacecraft, using the frequency channel the closest to that of JENRAGE. 
 
For each radio burst, several comparison can be done.:
- compare the peak flux density between the spacecraft;
- fit a gaussian profile on the times-series and compare the fitted peak flux density;
- interpolate the other spacecraft times-series using the JUICE data timestamps and compare the interpolated values to JUICE data.

In case the heliocentric longitude of the spacecraft are larger than a few degrees, we can use the model developed by Musset et al. (2021) to evaluate the solar burst intensity in the direction of the JUICE spacecraft, using the 3 other spacecraft calibrated intensities. 

## Observations
The current common observations dates are: 
- 2024-01-26: 2 events

## References
- Musset, S., et al. (2021). _Simulations of radio-wave anisotropic scattering to interpret type III radio burst data from Solar Orbiter, Parker Solar Probe, STEREO, and Wind_. A&A 656, A34. [doi:10.1051/0004-6361/202140998](https://doi.org/10.1051/0004-6361/202140998)
- Vecchio, A., et al. (2021). _Solar Orbiter/RPW antenna calibration in the radio domain and its application to type III burst observations_. A&A 656, A33. [doi:10.1051/0004-6361/202140988](https://doi.org/10.1051/0004-6361/202140988)
